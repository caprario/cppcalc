#include <iostream>

namespace math {


	double square(double n)
	{
		return n*n;
	}
	double sqrt(double n)
	{
		auto old = 0.0;
		auto guess = 1.0;

		while (old != guess) {
			old = guess;
			guess = (guess + n / guess) / 2;
		}
		return guess;
	}

	double power(double b, int exp) {
		auto answer = 1.0;

			for (auto i = 0; i < exp; +i) {
				answer *= b;
			}
	}


}


int main()
{
   using namespace math;
   //std::cout << "I am A calculator!\n\n\n\n\n";
   //std::cout << "+----------------------------------+";
   //std::cout << "| O resultado eh" << square(42) << '\n';
   //std::cout << "+----------------------------------+";
   //std::cout << math::square(42) << '\n';
   std::cout << math::sqrt(square(42)) << '\n';
   return 0; // nao eh necessario mas avisa que a coisa rodou bem.
}